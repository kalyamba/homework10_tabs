document.addEventListener("DOMContentLoaded", function () {

    let tabs = document.querySelectorAll(".tabs-title");
    let contents = document.querySelectorAll(".tabs-content li");


    for (let i = 1; i < contents.length; i++) {
        contents[i].style.display = "none";
    }


    tabs.forEach(function (tab, index) {
        tab.addEventListener("click", function () {

            tabs.forEach(function (tab) {
                tab.classList.remove("active");
            });

            this.classList.add("active");


            contents.forEach(function (content) {
                content.style.display = "none";
            });

            contents[index].style.display = "block";
        });
    });
});
